// MAP
let map = L.map('map', {
    center: [48.148598,17.107748],
    zoom: 14
});

L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png',{
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

// VARIABLES
let loader = document.createElement('img');
    loader.setAttribute('id','loader');
    loader.setAttribute('src','static/images/search.gif');
    loader.setAttribute('alt','search');
let markers = L.layerGroup().addTo(map);
let restaurants = L.layerGroup().addTo(map);
let tooltip = L.layerGroup().addTo(map);
let polygons = L.layerGroup().addTo(map);
let roads = L.layerGroup().addTo(map);
let iconOptions = {iconUrl: 'static/images/restaurant-icon.png',iconAnchor: [20, 41]};
let customIcon = L.icon(iconOptions);
let markRestaurant = {
   clickable: true,
   draggable: true,
   icon: customIcon
};
let sideTable = document.getElementById('side-table').getElementsByTagName('tbody')[0];
let csrftoken = getCookie('csrftoken');


// EVNETS
map.on('click', onMapClick);


// FUNCTIONS
async function findRestaurants(location) {
     while (sideTable.firstChild) {
        sideTable.removeChild(sideTable.firstChild);
    }

    let response = await fetch('/api/restaurant/', {
        method: 'post',
        headers: {'Content-Type': 'application/json', 'Origin':'http://pdt.home/',"X-CSRFToken":csrftoken},
        body: JSON.stringify(location)
    });

    if (response.ok) {
        let re_json = await response.json();
        for (let i = 0; i < re_json.length ; i++) {
            let icon = L.marker(re_json[i][1].coordinates,markRestaurant).addTo(restaurants);
            let row =  sideTable.insertRow();
            row.innerHTML = "<td>"+parseInt(i+1)+"</td><td>"+re_json[i][0]+"</td>";
            row.addEventListener('mouseenter', highlightRow);
            row.addEventListener('mouseleave', highlightRow);
            row.addEventListener('click', (elem) => restaurantDetail(elem,location,re_json[i][1].coordinates,re_json[i][0]));
            icon.addEventListener('click', (elem) => restaurantDetail(elem,location,re_json[i][1].coordinates,re_json[i][0]));
        }
    } else {
        alert("HTTP-Error: " + response.status);
    }
}

async function markRoad(params, name) {

    let response = await fetch('/api/roads/', {
        method: 'post',
        headers: {'Content-Type': 'application/json', 'Origin':'http://pdt.home/',"X-CSRFToken":csrftoken},
        body: JSON.stringify(params)
    });

    if (response.ok) {
        let re_json = await response.json();
        let polyline = L.polyline(re_json, {color: 'red'}).addTo(roads);
        restaurants.clearLayers();
        L.popup().setLatLng([params['lat_stop'],params['lng_stop']])
            .setContent("<b>"+name+"</b><br><button id='parking'>Nájsť parkovanie!</button>")
            .addTo(tooltip);
        let parkButton = document.getElementById('parking');
        parkButton.addEventListener("click",() => parkingButton([params['lat_stop'],params['lng_stop']],re_json));
        map.fitBounds(polyline.getBounds());
    } else {
        alert("HTTP-Error: " + response.status);
    }
}

async function findParking(params) {

    let response = await fetch('/api/parking/', {
        method: 'post',
        headers: {'Content-Type': 'application/json', 'Origin':'http://pdt.home/',"X-CSRFToken":csrftoken},
        body: JSON.stringify(params)
    });

    if (response.ok) {
        let re_json = await response.json();
        if(re_json.length === 0){
            alert("Ľutujeme ale pri tejto reštaurácii sa nenachádza parkovisko!");
        } else {
             for (let i = 0; i < re_json.length ; i++) {
                L.polygon(re_json[i][0].coordinates).addTo(polygons);
             }
        }

    } else {
        alert("HTTP-Error: " + response.status);
    }
}

function parkingButton(location, road) {
    polygons.clearLayers();
    let params = {
        "lat":location[0],
        "lng":location[1],
        "road":road
    };
    findParking(params).then();
}

function onMapClick(e) {
    markers.clearLayers();
    restaurants.clearLayers();
    roads.clearLayers();
    polygons.clearLayers();
    L.marker(e.latlng).addTo(markers);
    findRestaurants(e.latlng).then();
}

function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        let cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            let cookie = cookies[i].trim();

            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function highlightRow() {
    if(this.className === 'highlight'){
        this.classList.remove('highlight');
    } else {
        this.classList.add('highlight');
    }
}

async function restaurantDetail(elem,start,finish,name) {
    elem.target.appendChild(loader);
    let params = {
          "lng_start": start['lng'],
          "lat_start": start['lat'],
          "lng_stop": finish[1],
          "lat_stop": finish[0],
    };
    roads.clearLayers();
    polygons.clearLayers();
    tooltip.clearLayers();
    await markRoad(params,name).then();
    elem.target.removeChild(loader);
}
