from django.urls import path, include
from bratislava import views
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title="Bratislava map api")

urlpatterns = [
    path('', schema_view),
    path('restaurant/', views.Restaurant.as_view()),
    path('roads/', views.Road.as_view()),
    path('parking/', views.Parking.as_view()),

]

urlpatterns = format_suffix_patterns(urlpatterns)
