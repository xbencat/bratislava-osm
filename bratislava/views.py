from django.http import Http404, JsonResponse
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rest_framework_swagger import renderers
from django.db import connection
from bratislava.serializers import *
from django.core.serializers import serialize


class Restaurant(GenericAPIView):
    renderer_classes = [
        renderers.OpenAPIRenderer,
        renderers.SwaggerUIRenderer,
        renderers.JSONRenderer
    ]
    serializer_class = RestaurantSerializer
    @staticmethod
    def raw_query():
        return "SELECT name, \
                    st_asgeojson(ST_FlipCoordinates(st_transform(way, 4326)::geometry))::json as point, \
                    st_distance(way,ST_Transform(st_setsrid(st_point(%s,%s),4326),3857)) as distance\
                FROM planet_osm_point\
                WHERE amenity = 'restaurant' \
                    AND name IS NOT NULL\
                    AND st_dwithin(way,ST_Transform(st_setsrid(st_point(%s,%s),4326),3857),1200)\
                ORDER BY distance"

    def post(self, request, format=None):
        location = [request.data['lng'], request.data['lat']]
        location.extend(location)

        with connection.cursor() as cursor:
            cursor.execute(self.raw_query(), location)
            rows = cursor.fetchall()

        return JsonResponse(rows, safe=False)


class Road(GenericAPIView):
    renderer_classes = [
        renderers.OpenAPIRenderer,
        renderers.SwaggerUIRenderer,
        renderers.JSONRenderer
    ]
    serializer_class = RoadSerializer
    @staticmethod
    def raw_query():
        return "SELECT w.source, st_asgeojson(ST_FlipCoordinates(st_transform(the_geom, 4326)::geometry))::json \
                    FROM pgr_astar( \
                       'SELECT gid as id, source, target, cost, reverse_cost, x1,y1,x2,y2 FROM ways', \
                        (SELECT source FROM ways \
                        ORDER BY st_distance(the_geom,st_setsrid(st_point(%s, %s),4326)) \
                        LIMIT 1), \
                       (SELECT source FROM ways \
                        ORDER BY st_distance(the_geom,st_setsrid(st_point(%s, %s),4326)) \
                        LIMIT 1),directed := false, heuristic := 2 \
                    )AS pt \
                JOIN ways w ON pt.edge = w.gid\
                ORDER BY pt.seq;"

    def post(self, request, format=None):
        locations = [request.data['lng_start'],
                     request.data['lat_start'],
                     request.data['lng_stop'],
                     request.data['lat_stop']]
        road = list()
        source = dict()
        with connection.cursor() as cursor:
            cursor.execute(self.raw_query(), locations)
            rows = cursor.fetchall()

        for row in rows:
            if row[0] not in source:
                source[row[0]] = True
                for each in row[1]['coordinates']:
                    if each not in road:
                        road.append(each)

        return JsonResponse(road, safe=False)


class Parking(GenericAPIView):
    renderer_classes = [
        renderers.OpenAPIRenderer,
        renderers.SwaggerUIRenderer,
        renderers.JSONRenderer
    ]
    serializer_class = ParkingSerializer
    @staticmethod
    def raw_query(points):
        return "SELECT st_asgeojson(ST_FlipCoordinates(st_transform(py.way, 4326)::geometry))::json as polygon \
                FROM planet_osm_polygon py \
                WHERE (py.amenity = 'parking' OR py.amenity = 'parking_space') AND \
                    st_dwithin(py.way, ST_Transform(st_setsrid(st_point(%s,%s), 4326), 3857), 200) \
                ORDER BY st_distance(py.way,ST_Transform(st_setsrid(st_makeline(array ["+points+"]),4326),3857)) \
                LIMIT 2;"

    def post(self, request, format=None):
        road = request.data['road']
        params = list()
        params.append(request.data['lng'])
        params.append(request.data['lat'])
        points = ""
        for each in road:
            points += "st_makepoint("+str(each[1])+","+str(each[0])+"),"

        with connection.cursor() as cursor:
            cursor.execute(self.raw_query(points[:-1]), params)
            rows = cursor.fetchall()

        return JsonResponse(rows, safe=False)
