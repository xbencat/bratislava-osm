from django.apps import AppConfig


class BratislavaConfig(AppConfig):
    name = 'bratislava'
