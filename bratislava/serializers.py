from abc import ABC

from django.contrib.auth.models import User, Group
from rest_framework import serializers
from rest_framework_gis.serializers import GeoFeatureModelSerializer
from bratislava.models import *


class CoordinateField(serializers.Field):

    def to_representation(self, value):
        ret = {
            "x": value.x_coordinate,
            "y": value.y_coordinate
        }
        return ret

    def to_internal_value(self, data):
        ret = {
            "x_coordinate": data["x"],
            "y_coordinate": data["y"],
        }
        return ret


class RestaurantSerializer(serializers.Serializer):
    lng = serializers.FloatField()
    lat = serializers.FloatField()
    # location = CoordinateField(source='*')


class RoadSerializer(serializers.Serializer):
    lng_start = serializers.FloatField()
    lat_start = serializers.FloatField()
    lng_stop = serializers.FloatField()
    lat_stop = serializers.FloatField()
    # location = CoordinateField(source='*')


class ParkingSerializer(serializers.Serializer):
    lng = serializers.FloatField()
    lat = serializers.FloatField()
    road = serializers.ListField()
    # location = CoordinateField(source='*')