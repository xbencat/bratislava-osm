from django.urls import include, path
from django.views.generic.base import TemplateView

urlpatterns = [
    path('api/', include('bratislava.urls')),
    path('', TemplateView.as_view(template_name='index.html'), name="home"),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),

]

